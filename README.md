# WebGameSDK
[![](https://jitpack.io/v/com.gitlab.webgamesdk/gamesdk.svg)](https://jitpack.io/#com.gitlab.webgamesdk/gamesdk)

## Used libraries

- [Javalin](https://javalin.io/)
- [JSON-java](https://github.com/stleary/JSON-java)
- [jnanoid](https://github.com/aventrix/jnanoid)

*TODO: add description*

## Planed features

- Error safety for Rooms
- Unified protocol