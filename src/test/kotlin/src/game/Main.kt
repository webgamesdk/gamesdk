package src.game

import de.walamana.gamesdk.event.OnEvent
import de.walamana.gamesdk.ext.json
import de.walamana.gamesdk.ext.plus
import de.walamana.gamesdk.ext.toJSONArray
import de.walamana.gamesdk.ext.workingDirectory
import de.walamana.gamesdk.logger.ConsoleOutput
import de.walamana.gamesdk.logger.FileOutput
import de.walamana.gamesdk.logger.Logger
import de.walamana.gamesdk.player.Player
import de.walamana.gamesdk.room.Room
import de.walamana.gamesdk.server.Connection
import de.walamana.gamesdk.server.GameServer
import de.walamana.gamesdk.server.dls.QuickGameServer
import org.json.JSONObject
import java.io.File
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

val log = Logger().apply {
    outputs.add(ConsoleOutput())
    outputs.add(FileOutput(File(System.getProperty("user.dir"), "/log.txt")))
}

fun main() {

    QuickGameServer<TestRoom>{
        newRoom = {configuration, server -> TestRoom(configuration, server) }

        onGeneral<Any>("test") {
            log.d("called on general")
        }

        on<Connection>("connection"){
            it.on<String>("log"){
//                log.d("Log $it")
            }
        }

        log.info("Server started")



        staticFiles(workingDirectory + "/test")
    }
}


class TestRoom(configuration: JSONObject, server: GameServer<TestRoom>) : Room<TestPlayer>(configuration, server){

    var state = State.WAITING
    var board = arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0)

    override fun onJoin(player: TestPlayer){
        logger.d("Player $player joined the room")
        if(players.size == 1){
            player.isCircle = true
        }
        broadcast { onGetPlayers(it) }
    }

    @OnEvent
    fun onStart(player: Player){
        throw Exception("This is a stupid error")
        if(players.size != 2) return
        state = if(Math.random() > 0.5) State.SELECT_CIRCLE else State.SELECT_CROSS
        println("Game starting")
    }

    @OnEvent
    fun onSelect(p: Player, index: Int){
        val player = p as TestPlayer
        if(board[index] != 0) return
        println("Player $player selecting on $index, $state")

        if(state == State.SELECT_CROSS && !player.isCircle){
            board[index] = -1
            broadcast { it.send("select", json { put("index", index); put("symbol", "cross") }) }
            state = State.SELECT_CIRCLE
        }else if(state == State.SELECT_CIRCLE && player.isCircle){
            board[index] = 1
            broadcast { it.send("select", json { put("index", index); put("symbol", "circle") }) }
            state = State.SELECT_CROSS
        }
    }

    @OnEvent(event = "players get")
    override fun onGetPlayers(player: TestPlayer) {
        player.send("players", players.map { it.toJSON().apply { put("test", true) } }.toJSONArray())
    }

    enum class State{
        SELECT_CIRCLE,
        SELECT_CROSS,
        WAITING
    }

    override fun createNewPlayer(connection: Connection): TestPlayer = TestPlayer(connection)
}

class TestPlayer(connection: Connection) : Player(connection){

    var isCircle = false
    override fun toString(): String {
        return "src.game.TestPlayer(isCircle=$isCircle)"
    }

    override fun toJSON(): JSONObject = json {
        put("name", name)
        put("isCircle", isCircle)
    }

}