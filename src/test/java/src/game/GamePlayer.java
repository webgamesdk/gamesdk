package src.game;

import de.walamana.gamesdk.player.Player;
import de.walamana.gamesdk.server.Connection;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class GamePlayer extends Player {

    boolean isCircle = false;

    public GamePlayer(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public JSONObject toJSON() {
        JSONObject obj = super.toJSON();
        obj.put("isCircle", isCircle);
        return obj;
    }
}
