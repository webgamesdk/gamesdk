package src.game;

import de.walamana.gamesdk.server.GameServer;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class Server extends GameServer<GameRoom> {


    @NotNull
    @Override
    public GameRoom createNewRoom(@NotNull JSONObject configuration, @NotNull GameServer<GameRoom> server) {
        return new GameRoom(configuration, server);
    }
}
