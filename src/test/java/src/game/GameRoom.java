package src.game;

import de.walamana.gamesdk.event.OnEvent;
import de.walamana.gamesdk.room.Room;
import de.walamana.gamesdk.server.Connection;
import de.walamana.gamesdk.server.GameServer;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

public class GameRoom extends Room<GamePlayer> {

    public GameRoom(@NotNull JSONObject configuration, @NotNull GameServer<?> server) {
        super(configuration, server);
    }

    @OnEvent
    public void onJoin(GamePlayer player){
        System.out.println("Player " + player.getName() + " joined the room" );
        if(getPlayers().size() == 1){
            player.isCircle = true;
        }
        broadcast(gamePlayer -> {
            onGetPlayers(gamePlayer);
            return null;
        });

    }

    @OnEvent
    public void onStart(GamePlayer player){

    }

    @OnEvent
    public void onSelect(GamePlayer player, int index){

    }

    @OnEvent(event = "players get")
    @Override
    public void onGetPlayers(@NotNull GamePlayer player) {
        super.onGetPlayers(player);
    }

    enum State{
        SELECT_CIRCLE,
        SELECT_CROSS,
        WAITING
    }

    @NotNull
    @Override
    public GamePlayer createNewPlayer(@NotNull Connection connection) {
        return new GamePlayer(connection);
    }
}
