package de.walamana.gamesdk.util

import de.walamana.gamesdk.ext.json
import org.json.JSONObject

object JSON{

    fun error(code: Int, message: String): JSONObject = json {
        put("code", code)
        put("message", message)
    }

}
