package de.walamana.gamesdk.util

enum class Error(val event: String, val code: Int, val message: String){
    COULD_NOT_CREATE_ROOM("room create error", 0, "Could not create room");

    fun json() = JSON.error(code, message)
}