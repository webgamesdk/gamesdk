package de.walamana.gamesdk.util

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

abstract class TimeoutScheduler(val timeout: Long, val timeUnit: TimeUnit = TimeUnit.MILLISECONDS) {

    companion object{
        fun new(timeout: Long, timeUnit: TimeUnit = TimeUnit.MILLISECONDS, onTimeout: () -> Unit)
                = object : TimeoutScheduler(timeout, timeUnit) {
                        override fun onTimeout() {
                            onTimeout.invoke()
                        }
                    }
    }

    val scheduler = Executors.newSingleThreadScheduledExecutor()
    var future: ScheduledFuture<*>? = null

    fun start(){
        future = scheduler.schedule({ onTimeout() }, timeout, timeUnit)
    }

    fun stop(){
        future?.cancel(false)
    }

    abstract fun onTimeout()
}