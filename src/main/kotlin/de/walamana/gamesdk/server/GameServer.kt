package de.walamana.gamesdk.server

import de.walamana.gamesdk.event.EventEmitter
import de.walamana.gamesdk.ext.getOrNull
import de.walamana.gamesdk.ext.getStringOrNull
import de.walamana.gamesdk.logger.ConsoleOutput
import de.walamana.gamesdk.logger.Logger
import de.walamana.gamesdk.ext.json
import de.walamana.gamesdk.ext.mapJSONArray
import de.walamana.gamesdk.room.Room
import de.walamana.gamesdk.util.Error
import de.walamana.gamesdk.util.JSON
import io.javalin.Javalin
import io.javalin.http.staticfiles.Location
import io.javalin.websocket.WsCloseContext
import io.javalin.websocket.WsConnectContext
import org.eclipse.jetty.websocket.api.Session
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.lang.Exception
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.math.log

// TODO: Add Java constructor
abstract class GameServer<T : Room<*>>(
        val configuration: ServerConfiguration = ServerConfiguration()
) : EventEmitter(){

    val app = Javalin.create()

    var socketPath: String = "/socket"

    var connections = ConcurrentLinkedQueue<Connection>();

//    var roomClass: Class<*> = Room::class.java
    val rooms = arrayListOf<T>()

    val all = EventEmitter()

    var debug: Boolean = true

    val logger = Logger("internal").apply {
        outputs.add(object : ConsoleOutput(){
            override fun write(message: String, level: Logger.Level) {
                if(!debug) return
                super.write(message, level)
            }
        })
    }

    init{
        app.apply {
            config.showJavalinBanner = false
            ws(socketPath) {
                it.onConnect{ ctx -> onConnect(ctx) }
                it.onClose { ctx -> onClose(ctx) }
                it.onMessage { ctx -> onMessage(ctx.session, ctx.message()) }
            }
        }.start(configuration.port)
        init()
        setupEvents()
        logger.d("GameServer started")
    }


    open fun init(){ }

    /**
     * Sends a message to all connected clients
     */
    fun send(event: String, data: Any? = null){
        connections.forEach { it.send(event, data, Connection.Context.ALL) }
    }

    fun onRoomCreated(func: (room: Room.RoomCreatedResult) -> Unit) = on("room created", func)

    fun getRoom(id: String): T? = rooms.find { it.id.toString() == id }

    private fun setupEvents(){
        on<Connection>("connection", { con ->

            if(configuration.defaults.useRoomCreate){
                con.on<JSONObject>("room create"){ configuration ->
                    createRoom(con, configuration)
                }
            }

            if(configuration.defaults.useRoomJoin){
                con.on<JSONObject>("room join"){ data ->
                    if(con.room != null && con.room!!.id != data.getString("id")){
                        con.room!!.removePlayer(con)
                    }
                    val room = rooms.find { it.id.toString() == data.getString("id") }
                    if(room != null){
                        room.join(con, data.getJSONObject("data"))
                    }else {
                        con.send("room join error", json {
                            put("code", 0)
                            put("reason", "room does not exist")
                        })
                    }
                }
            }

            if(configuration.defaults.useRoomList){
                con.on("room list"){
                    con.send("room list", rooms.mapJSONArray { room ->
                        json {
                            put("id", room.id)
                            put("configuration", room.configuration)
                            put("playersCount", room.players.size)
                        }
                    })
                }
            }
        }, priority = 1)
    }



    private fun createRoom(con: Connection, configuration: JSONObject){
        val room: Room<*>?
        try{
            room = createNewRoom(configuration, this)
        }catch (ex: Exception){
            logger.error("A Room caused an exception while constructing: $ex")
            con.error(Error.COULD_NOT_CREATE_ROOM)
            return;
        }
        rooms.add(room)
        call("room created", Room.RoomCreatedResult(con, room))
        con.send("room created", room.id)
    }

    fun staticFiles(filePath: File){ staticFiles(filePath.absolutePath) }
    fun staticFiles(filePath: String){ app.config.addStaticFiles(filePath, Location.EXTERNAL) }

    inline fun <reified T> onGeneral(event: String, noinline func: (data: T) -> Unit): EventHandler<T> = on(event, func, Connection.Context.GENERAL)

    private fun onConnect(ctx: WsConnectContext){
        ctx.session.idleTimeout = Long.MAX_VALUE
        ctx.session.remote.sendString(JSONObject().apply {
            put("event", "register")
        }.toString())
    }

    private fun onClose(ctx: WsCloseContext){
        connections.find { it.session == ctx.session }?.also {
            it.startTimeout()
        }
    }

    private fun onMessage(session: Session, message: String){
        try{
            val packet = JSONObject(message)
            val context = packet.getString("context").split(":")
            val data = packet.getOrNull("data")
            val dataType = packet.getStringOrNull("dataType")
            val sessionId = packet.getStringOrNull("session_id")
            logger.d("\"${packet.getString("event")}\" DataType: $dataType")

            when(val event = packet.getString("event")){
                "register" -> registerSession(session, sessionId)
                else -> {
                    connections.find {
                        it.sessionId == sessionId
                    }?.also { con ->
                        when(Connection.Context.valueOf(context[0])){
                            Connection.Context.ALL -> con.callAny(event, data, dataType)
                            Connection.Context.ROOM -> {
                                getRoom(context[1])?.also { room ->
                                    try {
                                        room.call(con, event, data)
                                    }catch (er: Exception){
                                        logger.error("Exception $er: Destroying room ${room.id}")
                                        room.destroy()
                                    }
                                }
                            }
                            Connection.Context.GENERAL -> call(event, Connection.Context.GENERAL)
                        }
                    }
                }
            }
        }catch(ex: JSONException){
            logger.e("Invalid websocket message $message")
            ex.printStackTrace()
            session.disconnect()
        }
    }

    private fun registerSession(session: Session, sessionId: String?){
        if(sessionId == null || sessionId.isEmpty()){
            createConnection(session)
            return
        }
        logger.d("User trying to reconnect $sessionId")
        connections.find { it.sessionId == sessionId }?.reconnect(session) ?: createConnection(session)
    }

    private fun createConnection(session: Session){
        val con = Connection(this@GameServer, UUID.randomUUID().toString(), session)
        connections.add(con)
        call("connection", con)
        con.send("registered", con.sessionId, context = Connection.Context.ALL)
    }


    abstract fun createNewRoom(configuration: JSONObject, server: GameServer<T>): T
}
