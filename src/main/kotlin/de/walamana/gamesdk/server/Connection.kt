package de.walamana.gamesdk.server

import de.walamana.gamesdk.event.EventEmitter
import de.walamana.gamesdk.logger.Logger
import de.walamana.gamesdk.room.Room
import de.walamana.gamesdk.util.Error
import org.eclipse.jetty.websocket.api.Session
import org.json.JSONObject
import java.io.IOException
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class Connection(
    val server: GameServer<*>,
    val sessionId: String,
    var session: Session?
) : EventEmitter(){

    companion object{
        const val TIMEOUT = 10*1000L
    }

    val scheduler = Executors.newSingleThreadScheduledExecutor()
    var timeout: ScheduledFuture<*>? = null

    var mainContext: Context = Context.ALL
    
    var room: Room<*>? = null

    val log: Logger
    get() = server.logger


    fun send(event: String, data: Any? = null, context: Context = mainContext){
        session?.also {
            if(it.isOpen && timeout == null){
                try{
                    it.remote.sendString(JSONObject().apply {
                        put("event", event)
                        put("context", context.name)
                        put("session_id", sessionId)
                        put("data", if(data is Parsable) data.toJSON() else data)
                    }.toString())
                }catch(ex: IOException){
                    ex.printStackTrace()
                }
            }
        }
    }

    fun error(error: Error){
        send(error.event, error.json())
    }

    fun reconnect(session: Session){
        if(timeout != null){
            timeout!!.cancel(true)
            timeout = null
        }
        log.d("User reconnecting...")
        this.session = session
        call(Event.RECONNECT.name)
        send("reconnect", context = Context.ALL)
        // TODO: Implement
    }

    fun startTimeout(){
        log.d("Starting timeout...")
        call(Event.DISCONNECT.name)
        timeout = scheduler.postDelayed(TIMEOUT){
            destroy()
        }
    }

    fun destroy(){
        // TODO: Deletion
        server.connections.remove(this)
        call(Event.DESTROY.name)
    }

    // TODO: add ping to prevent zombie connections

    private fun ScheduledExecutorService.postDelayed(duration: Long, func: () -> Unit): ScheduledFuture<*> =
        schedule(func, duration, TimeUnit.MILLISECONDS)

    enum class Context{
        ALL,
        ROOM,
        GENERAL,
    }
    enum class Event{
        RECONNECT,
        DISCONNECT,
        DESTROY;
    }
}