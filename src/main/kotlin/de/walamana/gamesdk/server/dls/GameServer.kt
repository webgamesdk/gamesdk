package de.walamana.gamesdk.server.dls

import de.walamana.gamesdk.room.Room
import de.walamana.gamesdk.server.GameServer
import de.walamana.gamesdk.server.ServerConfiguration
import org.json.JSONObject


class QuickGameServer<T : Room<*>>(configuration: ServerConfiguration = ServerConfiguration(), func: QuickGameServer<T>.() -> Unit) : GameServer<T>(configuration) {

    init {
        func()
    }

    lateinit var newRoom: (configuration: JSONObject, server: GameServer<T>) -> T

    override fun createNewRoom(configuration: JSONObject, server: GameServer<T>): T = newRoom(configuration, server)
}