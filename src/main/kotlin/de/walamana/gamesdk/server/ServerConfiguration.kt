package de.walamana.gamesdk.server

data class ServerConfiguration (
        var defaults: ServerDefaults = ServerDefaults(),
        var port: Int = 4567
)

data class ServerDefaults (
        var useRoomCreate: Boolean = true,
        var useRoomJoin: Boolean = true,
        var useRoomList: Boolean = true
)