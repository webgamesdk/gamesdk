package de.walamana.gamesdk.logger

open class ConsoleOutput : Logger.Output{
    override fun getLogLevels(): List<Logger.Level> = listOf(Logger.Level.INFO, Logger.Level.VERBOSE, Logger.Level.ERROR, Logger.Level.DEBUG)
    override fun write(message: String, level: Logger.Level) {
        val color = when(level){
            Logger.Level.VERBOSE -> "\u001B[33m"
            Logger.Level.ERROR -> "\u001B[31m"
            Logger.Level.DEBUG -> "\u001B[32m"
            else -> ""
        }
        println("$color$message\u001B[0m")
    }
    override fun close() {}
}