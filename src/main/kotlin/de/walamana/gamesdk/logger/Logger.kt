package de.walamana.gamesdk.logger

import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Logger(val name: String = "main") {

    var outputs: ArrayList<Output> = arrayListOf()

    var timeFormat = SimpleDateFormat("HH:mm:ss")

    init {
        Runtime.getRuntime().addShutdownHook(Thread{
            close()
        })
    }


    fun l(message: String, level: Level, tag: String? = null) = log(message, level, tag)
    fun log(message: String, level: Level, tag: String? = null){
        val msg = "${prefix(level, tag)} $message"
        for(output in outputs){
            for(oLevel in output.getLogLevels()){
                if(oLevel == level){
                    output.write(msg, level)
                    break
                }
            }
        }
    }


    fun i(message: String, tag: String? = null) = info(message, tag)
    fun info(message: String, tag: String? = null) = log(message, Level.INFO, tag)

    fun v(message: String, tag: String? = null) = verbose(message, tag)
    fun verbose(message: String, tag: String? = null) = log(message, Level.VERBOSE, tag)

    fun e(message: String, tag: String? = null) = error(message, tag)
    fun error(message: String, tag: String? = null) = log(message, Level.ERROR, tag)

    fun d(message: String, tag: String? = null) = debug(message, tag)
    fun debug(message: String, tag: String? = null) = log(message, Level.DEBUG, tag)



    fun prefix(level: Level, tag: String? = null): String
            = "[${timeFormat.format(Date())} ${if(name.isNotEmpty()) "$name:" else ""}${level.name}${ if(tag != null) ":$tag" else ""}]"

    fun closeOutput(output: Output){
        output.close()
        outputs.remove(output)
    }

    fun close(){
        val toClose = outputs.clone() as ArrayList<Output>
        for(output in toClose){
            closeOutput(output)
        }
    }

    enum class Level(val index: Int){
        INFO(0),
        VERBOSE(1),
        ERROR(2),
        DEBUG(3);

        companion object{
            fun fromIndex(index: Int): Level = when(index){
                0 -> INFO
                1 -> VERBOSE
                2 -> ERROR
                3 -> DEBUG
                else -> DEBUG
            }
        }
    }

    interface Output{
        fun getLogLevels(): List<Level>
        fun write(message: String, level: Level)
        fun close()
    }
}