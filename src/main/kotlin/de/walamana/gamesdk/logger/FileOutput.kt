package de.walamana.gamesdk.logger

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.OpenOption
import java.nio.file.StandardOpenOption

open class FileOutput(
        private val file: File,
        override: Boolean = true
) : Logger.Output {
    init{
        if(!file.exists()) {
            file.createNewFile()
        }else if(override){
            file.delete()
            file.createNewFile()
        }
    }
    override fun getLogLevels(): List<Logger.Level> = listOf(Logger.Level.INFO, Logger.Level.VERBOSE, Logger.Level.ERROR, Logger.Level.DEBUG)
    override fun write(message: String, level: Logger.Level) {
        if(!file.canWrite()) return
        Files.write(file.toPath(), (message + "\n").toByteArray(Charsets.UTF_8), StandardOpenOption.APPEND)
    }
    override fun close() {}
}