package de.walamana.gamesdk.room

import com.aventrix.jnanoid.jnanoid.NanoIdUtils
import de.walamana.gamesdk.event.EventEmitter
import de.walamana.gamesdk.event.OnEvent
import de.walamana.gamesdk.ext.json
import de.walamana.gamesdk.ext.toJSONArray
import de.walamana.gamesdk.logger.ConsoleOutput
import de.walamana.gamesdk.logger.Logger
import de.walamana.gamesdk.player.Player
import de.walamana.gamesdk.server.Connection
import de.walamana.gamesdk.server.GameServer
import de.walamana.gamesdk.util.TimeoutScheduler
import org.json.JSONObject
import java.util.*
import java.util.concurrent.TimeUnit

abstract class Room<T : Player>(
    var configuration: JSONObject,
    val server: GameServer<*>
) : EventEmitter(){

    companion object{
        private const val PLAYER_JOIN = "PLAYER_JOIN"
        private const val PLAYER_LOST_CONNECTION = "PLAYER_LOST_CONNECTION"
        private const val PLAYER_RECONNECT = "PLAYER_RECONNECT"
        private const val PLAYER_QUIT = "PLAYER_QUIT"

        private const val DESTROY = "DESTROY"
    }

    val timeout = TimeoutScheduler.new(10, TimeUnit.MINUTES){
        destroy()
    }

    val id = NanoIdUtils.randomNanoId(NanoIdUtils.DEFAULT_NUMBER_GENERATOR, NanoIdUtils.DEFAULT_ALPHABET, 8)
    val players = arrayListOf<T>()

    var logger = Logger("r$id").apply {
        outputs.add(object : ConsoleOutput(){
            override fun write(message: String, level: Logger.Level) {
                if(!server.debug) return
                super.write(message, level)
            }
        })
    }

    fun join(connection: Connection, data: JSONObject?){
        // Join is rejected if player with the same connection is already registered. Rejoining is handled by onReconnect
        val player = players.find { it.connection == connection }
        if(player != null) {
            connection.send("room reconnect", json{
                put("id", id)
                put("configuration", configuration)
            })
            onReconnect(player)
        }else if(onRequestJoin(connection, data)){
            initializeNewPlayer(connection)
        }else{
            connection.send("error", "You cannot join this room")
        }
    }

    private fun initializeNewPlayer(connection: Connection){
        val player = createNewPlayer(connection)
        players.add(player)
        connection.on(Connection.Event.DESTROY.name){
            removePlayer(player)
        }
        connection.on(Connection.Event.DISCONNECT.name){
            onDisconnect(player)
        }
        connection.on("room quit"){
            removePlayer(player)
        }
        connection.send("room joined", json {
            put("id", id)
            put("configuration", configuration)
        })
        connection.room = this
        onJoin(player)
    }


    // Default functions. You shouldn't listen to them by yourself

    /**
     * Is called before a player is allowed to join
     * @return Returns true if the player is allowed to join the room
     */
    open fun onRequestJoin(connection: Connection, data: JSONObject?): Boolean{ return true }

    /**
     * Is called when a player joins the room
     */
    open fun onJoin(player: T){}

    /**
     * Is called when a player left the room
     */
    open fun onQuit(player: T){
        if(players.size == 0){
            destroy()
        }
    }

    /**
     * Is called when a player disconnects from its session. This doesn't necessarily mean the player wants to quit.
     */
    open fun onDisconnect(player: T){}

    /**
     * Is called when a player successfully reconnects to its session
     */
    open fun onReconnect(player: T){}

    /**
     * Is called before the room is being destroyed and removed from the room list
     */
    open fun onDestroy(){}


    fun removePlayer(connection: Connection){
        removePlayer(getPlayer(connection) ?: return)
    }

    fun removePlayer(player: T){
        logger.d("Player ${player.name} quits room")
        player.connection.room = null
        players.remove(player)
        onQuit(player)
        if(players.size == 0){
            destroy()
        }
    }

    fun destroy(){
        broadcast { it.send(DESTROY) }
        onDestroy()
        server.rooms.remove(this)
    }


    @OnEvent(event = "players get")
    open fun onGetPlayers(player: T){
        player.send("players", players.toJSONArray())
    }








    fun getPlayer(connection: Connection): T? = players.find { it.connection.sessionId == connection.sessionId }

    fun broadcast(func: (player: T) -> Unit){
        this.players.forEach {
            func(it)
        }
    }

    fun call(player: T, event: String, data: Any?){
        this::class.java.methods.forEach { method ->
            if(method.isAnnotationPresent(OnEvent::class.java)){
                val annotation = method.getAnnotation(OnEvent::class.java)
                val aEvent =
                        if(annotation.event == "")
                            method.name
                                    .removePrefix("on")
                                    .split("(?=\\p{Lu})".toRegex())
                                    .joinToString(" "){ it.toLowerCase() }
                                    .trim()
                        else annotation.event
                if(aEvent == event){
                    when(method.parameterCount){
                        1 -> method.invoke(this, player)
                        2 -> {
                            // FIXME: Check, if second parameters is nullable
                            if(data == null) return // If the developer requests data, he should manage to retrieve data, otherwise someone nasty is there
                            method.invoke(this, player, data)
                        }
                    }
                }
            }
        }
    }

    fun call(connection: Connection, event: String, data: Any?) {
        val player = getPlayer(connection) ?: return
        call(player, event, data)
    }

    data class RoomCreatedResult(val connection: Connection, val room: Room<*>)

    abstract fun createNewPlayer(connection: Connection): T

}