package de.walamana.gamesdk.ext

import org.json.JSONArray
import org.json.JSONObject

fun <T> Collection<T>.toArrayList(): ArrayList<T> = arrayListOf<T>().apply { addAll(this@toArrayList) }
inline fun <reified T> JSONArray.toArrayList(): ArrayList<T> = arrayListOf<T>().apply {
    for(el in 0 until this@toArrayList.length()){
        if(this@toArrayList.get(el) is T){
            add(this@toArrayList.get(el) as T)
        }
    }
}

fun JSONObject.assert(vararg keys: String): Boolean{
    for(key in keys){
        if(!has(key)) return false
    }
    return true
}

fun <T> JSONObject.get(key: String): T = this.get(key) as T
fun <T> JSONArray.get(index: Int): T = this.get(index) as T
fun <T, R> JSONArray.map(transform: (T) -> R): List<R> {
    return map<T, R> { transform(it as T) }
}
