package de.walamana.gamesdk.ext

import de.walamana.gamesdk.server.Parsable
import org.json.JSONArray
import org.json.JSONObject
import java.io.File

fun JSONObject.getOrNull(key: String): Any? = if(has(key)) get(key) else null
fun JSONObject.getStringOrNull(key: String): String? = if(has(key)) getString(key) else null
fun JSONObject.getJSONObjectOrNull(key: String): JSONObject? = if(has(key)) getJSONObject(key) else null
fun JSONObject.getJSONArrayOrNull(key: String): JSONArray? = if(has(key)) getJSONArray(key) else null


fun Collection<*>.toJSONArray(): JSONArray = JSONArray().also { json ->
    for(el in this){
        json.put(when(el){
            is Parsable -> el.toJSON()
            is JSONObject -> el
            is JSONArray -> el
            else -> el.toString()
        })
    }
}

fun <T> Collection<T>.mapJSONArray(transform: (el: T) -> Any): JSONArray = JSONArray().also { json ->
    for(el in this){
        json.put(transform(el))
    }
}

fun Array<*>.toJSONArray(): JSONArray = JSONArray().also { json ->
    for(el in this){
        json.put(when(el){
            is Parsable -> el.toJSON()
            is JSONObject -> el
            is JSONArray -> el
            else -> el.toString()
        })
    }
}


fun json(func: JSONObject.() -> Unit): JSONObject = JSONObject().apply(func)

val workingDirectory = File(System.getProperty("user.dir"))

operator fun File.plus(other: String): File = File(this, other)