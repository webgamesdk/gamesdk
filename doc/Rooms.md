# Rooms

## Default events

**To the server**

#### `Context.ROOM`:`room quit`
Is sent, when the client wants to quit the room

**To the client**

- `room joined`: Is sent, when the client successfully joined the room
- `room reconnect`: Is sent, when the client successfully reconnects to a room
```typescript
var data: {
    id: String,
    configuration: JSONObject
}
```
- ``